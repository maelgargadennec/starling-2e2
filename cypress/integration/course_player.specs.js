describe('The Course player', () => {

    before(() => {
        cy.clearLocalStorage();
        cy.login('mael.gargadennec@gmail.com', 'AbcdEf!@#123');
        cy.wait(2000);
    })

    beforeEach(() => {
        cy.log(localStorage.getItem('id_token'));
        cy.clearUserData('mael.gargadennec@gmail.com');
        cy.visit('/');
    })

    it('introduction', () => {
        cy.get('#course-player-start').click();

        cy.url().should('include', '/app/home/products');
        cy.url().should('include', '/start');

        const steps = [
            [{type: 'html', id: 'I01H0001', answer: {}}],
            [{type: 'mcq', id: 'I01MQ0001', hasFeedback: true, answer: {choice: 1}}],
            [{type: 'decision', id: 'I01N02D0001', noNextButton: true, answer: {choice: 0}}],
            [{type: 'poll', id: 'I01P0001', hasFeedback: true, answer: {choice: 0}}],
            [{type: 'workbook', id: 'I01W0001', answer: {text: 'This is a test'}}],
            [{type: 'video', id: 'I01V0001', answer: {}}],
            [{type: 'html', id: 'I01H0002', answer: {}}],
            [{type: 'html', id: 'I01H0003', answer: {}}],
            [{type: 'html', id: 'CU01H0002', answer: {}}],
            [{type: 'html', id: 'CU01H0005', answer: {}}],
            [{type: 'slider', id: 'CU01SL0001', answer: {percent: 0.6}}],
            [{type: 'workbook', id: 'CU01W0001', answer: {text: 'This is a test workbook'}}],
            [{type: 'mcq', id: 'CU01MQ0002', answer: {choice: 2}}],
            [{type: 'slider', id: 'CU01SL0005', answer: {percent: 0.99}}],
            [{type: 'mcq', id: 'CU01MQ0006', answer: {choice: 2}}],
            [{type: 'mcq', id: 'CU01MQ0007', answer: {choice: 2}}],
            [{type: 'mcq', id: 'CU01MQ0008', answer: {choice: 2}}],
            [{type: 'mcq', id: 'CU01MQ0009', answer: {choice: 1}}],
            [{type: 'mcq', id: 'CU01MQ0010', answer: {choice: 1}}],
            [
                {type: 'mcq', id: 'CU01MQ0011', answer: {choice: 0}},
                {type: 'workbook', id: 'CU01W0003', answer: {text: 'This is a test workbook'}}
            ],
            [{type: 'mcq', id: 'CU01MQ0012', answer: {choice: 1}}],
            [{type: 'html', id: 'CU01H0006', answer: {}}],
            // [{
            //     type: 'assessment', id: 'CU01AS0001', noNextButton: true,
            //     answer: {
            //         choices: [{choice: 1}, {choice: 1}]
            //     }
            // }]
        ];

        steps.forEach(group => {
            group.forEach((element, index) => {
                cy.checkElement(index, element);
                cy.executeElement(index, element);
            })
            if (!group.some(el => el.noNextButton)) {
                if (group.every(el => !el.hasFeedback)) {
                    cy.nextElement();
                } else {
                    cy.nextElementFeedback();
                }
            }
        })
    })

});
