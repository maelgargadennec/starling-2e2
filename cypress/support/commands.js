// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })


Cypress.Commands.add("login", (email, password) => {
    cy.clearLocalStorage();

    cy.visit("/#/landing/sign_in");

    cy.wait(800);

    cy.get('#field-username')
        .type(email);

    cy.get('#field-password')
        .type(password);

    cy.get('#btn-do-login')
        .click();
});

Cypress.Commands.add('clearUserData', (username) => {
    cy.request({
        method: 'DELETE',
        url: `https://admin-staging.starlingminds.com/api/userDataReset/${username}`,
        headers: {
            Authorization: localStorage.getItem('id_token')
        }
    });
});
