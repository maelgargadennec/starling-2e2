Cypress.Commands.add('executeElement', (index, expected) => {

    if (expected.type === 'html') {
        cy.htmlElement(index, expected);
    }
    if (expected.type === 'mcq') {
        cy.mcqElement(index, expected);
    }
    if (expected.type === 'poll') {
        cy.pollElement(index, expected);
    }
    if (expected.type === 'decision') {
        cy.decisionElement(index, expected);
    }
    if (expected.type === 'workbook') {
        cy.workbookElement(index, expected);
    }
    if (expected.type === 'video') {
        cy.videoElement(index, expected);
    }
    if (expected.type === 'slider') {
        cy.sliderElement(index, expected);
    }
    if (expected.type === 'assessment') {
        cy.assessmentElement(index, expected);
    }
});


Cypress.Commands.add("htmlElement", (index, element) => {
});

Cypress.Commands.add("mcqElement", (index, element) => {
    cy.get(`#element-${index} button[choice=${element.answer.choice}]`).click();
});

Cypress.Commands.add("decisionElement", (index, element) => {
    cy.get(`#element-${index} a[choice=${element.answer.choice}]`).click();
});

Cypress.Commands.add("pollElement", (index, element) => {
    cy.get(`#element-${index} button[choice=${element.answer.choice}]`).click();
});

Cypress.Commands.add("workbookElement", (index, element) => {
    cy.get(`#element-${index} textarea`).type(element.answer.text);
});

Cypress.Commands.add("videoElement", (index, element) => {
});

Cypress.Commands.add("sliderElement", (index, element) => {
    cy.get(`#element-${index} input[role="slider"]`).clickVSlider(element.answer.percent);
});

Cypress.Commands.add("assessmentElement", (index, element) => {

});
