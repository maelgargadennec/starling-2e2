Cypress.Commands.add('checkElement', (index, expected) => {
    cy.get(`#element-${index} .element-marker`).should('have.attr', 'element-type', expected.type);
    cy.get(`#element-${index} .element-marker`).should('have.attr', 'element-id', expected.id);
});


