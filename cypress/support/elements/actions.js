

Cypress.Commands.add("skipElement", () => {
    cy.get('#course-player-btn-skip').click();
});

Cypress.Commands.add("nextElement", () => {
    cy.get('#course-player-btn-next').click();
});

Cypress.Commands.add("nextElementFeedback", () => {
    cy.get('#course-player-btn-next-feedback').click();
});
