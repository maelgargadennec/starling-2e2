Cypress.Commands.add('clearUserData', (username) => {
    cy.request({
        method: 'DELETE',
        url: `https://admin-staging.starlingminds.com/api/userDataReset/${username}`,
        headers: {
            Authorization: localStorage.getItem('id_token')
        }
    });
});

Cypress.Commands.add('clickVSlider', {prevSubject: true}, (subject, percentFromLeft) => {
    if (percentFromLeft >= 1) {
        percentFromLeft = 0.99;
    }
    const sliderWidth = subject.width()
    const sliderHeight = subject.height()
    const pixelsFromLeft = percentFromLeft * sliderWidth
    const pixelsFromTop = 0.5 * sliderHeight
    cy.wrap(subject).click(pixelsFromLeft, pixelsFromTop)
})
